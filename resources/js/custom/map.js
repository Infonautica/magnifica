function initMap(addressObject, informationHtml) {
  var coordinates = {lat: addressObject.geo[0], lng: addressObject.geo[1]};
  var map = new google.maps.Map(document.getElementById('contacts__map'), {
    zoom: 17,
    center: coordinates
  });

  var htmlContent = informationHtml;

  var infowindow = new google.maps.InfoWindow({
    content: '<div class="contacts__map-baloon">' + htmlContent + '</div>'
  });

  var marker = new google.maps.Marker({
    position: coordinates,
    map: map,
    title: 'Показать информацию',
     icon: '/img/elem/marker.png'
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
}