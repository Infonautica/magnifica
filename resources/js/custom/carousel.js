function initCarousel() {
	var carousel = $(".spin__carousel");
	carousel.owlCarousel({
		loop: true,
		items: 3,
		center: true,
		autoplay: false,
		dots: true
	});

	$(".spin__carousel-arrow--prev").on('click', function(event) {
		carousel.trigger('prev.owl.carousel');
		event.preventDefault();
	});

	$(".spin__carousel-arrow--next").on('click', function(event) {
		carousel.trigger('next.owl.carousel');
		event.preventDefault();
	});
}