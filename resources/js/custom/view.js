// Empty options object
var mainData;

// Get data from server
$.ajax({
	method: 'post',
	url: '/api/all',
	success: function(response) {
		var data = response;

		// Init Vue.js with data
		initVue(data);
	}
});


// Main function
function initVue(data) {
	mainData = data;

	// Promo section
	var Promo = new Vue({
		el: '#promo',
		data: {
			promo: data.promo
		}
	});

	// Promo text on red background
	var PromoAbout = new Vue({
		el: '#promo__about',
		data: {
			promo: data.promo
		}
	});

	// Services
	var Services = new Vue({
		el: '#services',
		data: {
			services: data.services
		}
	});

	// Feedbacks
	var Feedbacks = new Vue({
		el: '#feedbacks',
		data: {
			feedbacks: data.feedbacks
		}
	});

	// Photos
	var Photos = new Vue({
		el: '#spin',
		data: {
			photos: data.photos
		}
	});

	console.log(data);
	console.log(data.photos);

	// Footer
	var Footer = new Vue({
		el: '#footer',
		data: {
			footer: data.contacts
		}
	});

	// Popups
	var Popups = new Vue({
		el: '#popups',
		data: {
			prices: data.prices
		}
	});

	// Init plugins
	initMap(data.contacts.address, data.contacts.info);
	initCarousel();
	popups();
}