// Popups
function popups() {
	// Popup opening
	$('.open-popup').click(function (e) {
		e.preventDefault();
		var popup = ('#' + $(this).attr('data-win')),
				p = $(popup),
				p__height = p.height(),
				p__width = p.width(),
				$win_height = $(window).height();
		$('.overlay').fadeIn();
		$(popup)
				.show()
				.css({
					'top': -p__height,
					'margin-left': -(p__width/2),
					'margin-top': -(p__height/2)
				})
				.animate({
					'top': ($win_height/2)
				});
	});
	// Popup closing
	$('.overlay, .popup__close').click(function(e){
		e.preventDefault();
		if ($('.popup').is(':visible')) {
			$('.overlay, .popup').fadeOut('100');
		}
	});
}