'use strict';

const keystone = require('keystone')
	, Types = keystone.Field.Types;
	
var Contacts = new keystone.List('Contacts', {
	defaultColumns: 'title',
	map: { name: 'title' }
});

Contacts.add({
	title: { type: Types.Text, label: 'Контакты', default: 'Контакты' },
	info: { type: Types.Html, wysiwyg: true, label: 'Текст в маркере', default: '<p>Текст в маркере</p>' },
	address: { 
		type: Types.Location,
		defaults: {
			state: 'Moscow',
			country: 'Russia'
		} 
	},
	footer: { type: Types.Html, wysiwyg: true, label: 'Текст в подвале', default: '<p>Магнифика © 2016</p>' }
});

Contacts.register();
