'use strict';

const keystone = require('keystone')
	, Types = keystone.Field.Types;
	
var PricesLady = new keystone.List('PricesLady', {
	defaultColumns: 'title, cost',
	map: { name: 'title' }
});

PricesLady.add({
	title: { type: Types.Text, label: 'Услуга', default: 'Стрижка' },
	cost: { type: Types.Text, label: 'Стоимость', default: '500 р.' }
});

PricesLady.register();
