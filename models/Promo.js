'use strict';

const keystone = require('keystone')
	, Types = keystone.Field.Types;
	
var Promo = new keystone.List('Promo', {
	defaultColumns: 'title',
	map: { name: 'title' },
	nocreate: true,
	nodelete: true
});

Promo.add({
	title: { type: Types.Text, label: 'Промо секция', default: 'Промо секция' },
	background: {
		type: Types.LocalFile, label: 'Фон услуги',
		dest: keystone.get('module root') + '/client/public/files/promo/',
		prefix: '/files/promo',
		allowedTypes: ['image/gif','image/jpeg','image/pjpeg','image/png','image/svg+xml','image/tiff'],
		format: function(item, file) {
			return '<img src="/files/promo/'+file.filename+'" style="max-width: 154px">';
		}
	},
	contacts: { type: Types.Html, wysiwyg: true, label: 'Контакты', default: 'Контакты в промо секции' },
	about: { type: Types.Text, label: 'О барбершопе', default: 'Текст на красном фоне' }
});

Promo.register();
