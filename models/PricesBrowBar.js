'use strict';

const keystone = require('keystone')
	, Types = keystone.Field.Types;
	
var PricesBrowBar = new keystone.List('PricesBrowBar', {
	defaultColumns: 'title, cost',
	map: { name: 'title' }
});

PricesBrowBar.add({
	title: { type: Types.Text, label: 'Услуга', default: 'Стрижка' },
	cost: { type: Types.Text, label: 'Стоимость', default: '500 р.' }
});

PricesBrowBar.register();
