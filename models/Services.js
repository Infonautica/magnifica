'use strict';

const keystone = require('keystone')
	, Types = keystone.Field.Types;
	
var Services = new keystone.List('Services', {
	defaultColumns: 'title',
	map: { name: 'title' }
});

Services.add({
	title: { type: Types.Text, label: 'Услуги', default: 'Услуги' },
	background: {
		type: Types.LocalFile, label: 'Фон услуги',
		dest: keystone.get('module root') + '/client/public/files/services/',
		prefix: '/files/services',
		allowedTypes: ['image/gif','image/jpeg','image/pjpeg','image/png','image/svg+xml','image/tiff'],
		format: function(item, file) {
			return '<img src="/files/services/'+file.filename+'" style="max-width: 154px">';
		}
	},
	icon: {
		type: Types.LocalFile, label: 'Иконка',
		dest: keystone.get('module root') + '/client/public/files/services/',
		prefix: '/files/services',
		allowedTypes: ['image/gif','image/jpeg','image/pjpeg','image/png','image/svg+xml','image/tiff'],
		format: function(item, file) {
			return '<img src="/files/services/'+file.filename+'" style="max-width: 154px">';
		}
	},
	btn: { type: Types.Text, label: 'Текст кнопки', default: 'Показать цены' }
});

Services.register();
