'use strict';

const keystone = require('keystone')
	, Types = keystone.Field.Types;
	
var PricesPodology = new keystone.List('PricesPodology', {
	defaultColumns: 'title, cost',
	map: { name: 'title' }
});

PricesPodology.add({
	title: { type: Types.Text, label: 'Услуга', default: 'Услуга' },
	cost: { type: Types.Text, label: 'Стоимость', default: '500 р.' }
});

PricesPodology.register();
