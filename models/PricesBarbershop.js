'use strict';

const keystone = require('keystone')
	, Types = keystone.Field.Types;
	
var PricesBarbershop = new keystone.List('PricesBarbershop', {
	defaultColumns: 'title, cost',
	map: { name: 'title' }
});

PricesBarbershop.add({
	title: { type: Types.Text, label: 'Услуга', default: 'Стрижка' },
	cost: { type: Types.Text, label: 'Стоимость', default: '500 р.' }
});

PricesBarbershop.register();
