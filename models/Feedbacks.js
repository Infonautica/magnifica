'use strict';

const keystone = require('keystone')
	, Types = keystone.Field.Types;
	
var Feedbacks = new keystone.List('Feedbacks', {
	defaultColumns: 'name',
	map: { name: 'name' }
});

Feedbacks.add({
	name: { type: Types.Text, label: 'Имя и фамилия', default: 'Вася Пупкин' },
	photo: {
		type: Types.LocalFile, label: 'Фотография',
		dest: keystone.get('module root') + '/client/public/files/feedbacks/',
		prefix: '/files/feedbacks',
		allowedTypes: ['image/gif','image/jpeg','image/pjpeg','image/png','image/svg+xml','image/tiff'],
		format: function(item, file) {
			return '<img src="/files/feedbacks/'+file.filename+'" style="max-width: 154px">';
		}
	},
	post: { type: Types.Text, label: 'Должность/Деятельность', default: 'Президент Марса' },
	text: { type: Types.Html, wysiwyg: true, label: 'Отзыв', default: '<p>Щикааарно</p>' }
});

Feedbacks.register();
