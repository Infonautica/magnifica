var gulp = require('gulp');
var sass = require('gulp-sass');

module.exports = function() {
	return gulp.src('./resources/scss/main.scss')
			.pipe(sass.sync().on('error', sass.logError))
			.pipe(gulp.dest('./client/public/css'));
};