var gulp = require('gulp');
var images = require('gulp-image-optimization');

module.exports = function(cb) {
    gulp.src(['./resources/img/**/*.png','./resources/img/**/*.jpg','./resources/img/**/*.gif','./resources/img/**/*.jpeg']).pipe(images({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
    })).pipe(gulp.dest('./client/public/img')).on('end', cb).on('error', cb);
};