var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');

module.exports = function () {
    return gulp.src('./client/public/css/main.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./client/public/css'));
};