var gulp = require('gulp');
var rename = require('gulp-rename');
var minifyCss = require('gulp-minify-css');

module.exports = function() {
	return gulp.src('./client/public/css/main.css')
			.pipe(minifyCss({compatibility: 'ie8'}))
			.pipe(rename('./client/public/css/main.min.css'))
			.pipe(gulp.dest('./'));
};