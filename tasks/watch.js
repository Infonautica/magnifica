var gulp = require('gulp');

module.exports = function() {
    gulp.watch("./resources/scss/**", ['minify']);
    gulp.watch("./resources/js/**", ['scripts']);
    gulp.watch("./resources/pages/**", ['jade']);
    gulp.watch("./resources/elements/**", ['jade']);
};