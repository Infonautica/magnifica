var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglifyjs');

module.exports = function() {
	return gulp.src('./resources/js/custom/*.js')
		.pipe(concat('custom.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./client/public/js/'));
};