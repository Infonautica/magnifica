var gulp = require('gulp');
var jade = require('gulp-jade');

module.exports = function() {
  var YOUR_LOCALS = {};
 
  gulp.src('./resources/pages/*.jade')
    .pipe(jade({
      locals: YOUR_LOCALS
    }))
    .pipe(gulp.dest('./client/public/'));
};