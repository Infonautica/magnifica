'use strict';

/**
 * Initialises the standard view locals
 */
exports.initLocals = function(req, res, next) {
	// console.log(req.body);
	// console.log(req.query);
	
	var locals = res.locals;
	locals.user = req.user;

	var sess = req.session;

	// save UTM marks
	if (req.query.utm_source) sess.utm_source = req.query.utm_source;
	if (req.query.utm_medium) sess.utm_medium = req.query.utm_medium;
	if (req.query.utm_term) sess.utm_term = req.query.utm_term;
	if (req.query.utm_content) sess.utm_content = req.query.utm_content;
	if (req.query.utm_campaign) sess.utm_campaign = req.query.utm_campaign;
	if (req.query.utm_keyword) sess.utm_keyword = req.query.utm_keyword;

	// save referer
	if (req.headers.referer && !sess.referer) sess.referer = req.headers.referer;

	// console.log(locals);
	// console.log(res.locals);
	// console.log(`referer: ${req.headers.referer}`);
	// console.log(sess);

	next();
};

/**
 * Server redirect from ip to domain
 */
exports.redirect = function(req, res, next) {
	// console.log(req.domain); // ?
	let reg = /^[\d\.]+$/;
	if (reg.test(req.hostname)) return res.redirect(301, 'http://kestler.ru/');
	next();
}