'use strict';

const keystone = require('keystone')
	, middleware = require('./middleware')
	, importRoutes = keystone.importer(__dirname);

// Common Middleware
// keystone.pre('routes', middleware.redirect);
// keystone.pre('routes', middleware.initLocals);

// Import Route Controllers
const routes = {
	api: importRoutes('./api')
};

// Setup Route Bindings
exports = module.exports = function(app) {
	
	// API
	// app.post('/api/practices', routes.api.practices.getAll); // get info about practices with filter
	// app.post('/api/estatePractice', routes.api.estatePractice.getAll); // get info about estatePractice with filter
	// app.post('/api/practices/:title', routes.api.practices.getOne); // get info about one project
	// app.post('/api/services', routes.api.services.getAll); // get info about services
	// app.post('/api/services/:title', routes.api.services.getOne); // get info about one service
	app.post('/api/all', routes.api.all.getAll); // get all info by one request

	// Send index file
	app.get('/*', function(req, res) {
		var indexFile = keystone.get('module root') + '/client/index.html';
		res.sendFile(indexFile, null, function(err) {
			if (err) return res.status(500).send(err.message);
			// console.log('sended index.html\n');
			res.status(200).end();
		});
	});
	
};
