'use strict';

const keystone = require('keystone')
	, async = require('async')
	, Promo = keystone.list('Promo')
	, Services = keystone.list('Services')
	, PricesLady = keystone.list('PricesLady')
	, PricesBarbershop = keystone.list('PricesBarbershop')
	, PricesBrowBar = keystone.list('PricesBrowBar')
	, Feedbacks = keystone.list('Feedbacks')
	, Contacts = keystone.list('Contacts')
	, PricesPodology = keystone.list('PricesPodology');

module.exports.getAll = function (req, res) {

	var allData = {
		promo: {},
		services: [],
		feedbacks: [],
		prices: {},
		contacts: {}
	};

	async.parallel({
		promo: function(callback) {
			Promo.model.findOne()
			.exec(function (err, promo) {
				if (err) return callback(err);
				var result = {
					contacts: promo.contacts,
					about: promo.about,
					background: promo.background
				};

				allData.promo = result;

				callback(err, result);
			});
		},
		services: function(callback) {
			Services.model.find()
			.exec(function (err, services) {
				if (err) return callback(err);
				let result = services.map(s => new Object({
					background: s.background,
					icon: s.icon,
					title: s.title,
					btn: s.btn
				}));

				allData.services = result;

				callback(err, result);
			});
		},
		feedbacks: function(callback) {
			Feedbacks.model.find()
			.exec(function (err, feedbacks) {
				if (err) return callback(err);
				let result = feedbacks.map(f => new Object({
					photo: f.photo,
					name: f.name,
					post: f.post,
					text: f.text
				}));

				allData.feedbacks = result;

				callback(err, result);
			});
		},
		lady: function(callback) {
			PricesLady.model.find()
			.exec(function (err, pricesLady) {
				if (err) return callback(err);
				let result = pricesLady.map(p => new Object({
					title: p.title,
					cost: p.cost
				}));

				allData.prices.lady = result;

				callback(err, result);
			});
		},
		podology: function(callback) {
			PricesPodology.model.find()
			.exec(function (err, pricesPodology) {
				if (err) return callback(err);
				let result = pricesPodology.map(p => new Object({
					title: p.title,
					cost: p.cost
				}));

				allData.prices.podology = result;

				callback(err, result);
			});
		},
		barbershop: function(callback) {
			PricesBarbershop.model.find()
			.exec(function (err, pricesBarbershop) {
				if (err) return callback(err);
				let result = pricesBarbershop.map(p => new Object({
					title: p.title,
					cost: p.cost
				}));

				allData.prices.barbershop = result;

				callback(err, result);
			});
		},
		browbar: function(callback) {
			PricesBrowBar.model.find()
			.exec(function (err, browbar) {
				if (err) return callback(err);
				let result = browbar.map(p => new Object({
					title: p.title,
					cost: p.cost
				}));

				allData.prices.browbar = result;

				callback(err, result);
			});
		},
		contacts: function(callback) {
			Contacts.model.findOne()
			.exec(function (err, contacts) {
				if (err) return callback(err);
				var result = {
					info: contacts.info,
					address: contacts.address,
					footer: contacts.footer
				};

				allData.contacts = result;

				callback(err, result);
			});
		}
	}, function (err, data) {
		if (err) res.status(500).json(err.message);
		console.log(allData);
		res.status(200).json(allData);
	});
};