'use strict';

// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').load();

// Require keystone
const keystone = require('keystone');

keystone.init({
	
	'env': process.env.NODE_ENV || 'development',
	'mongo': process.env.MONGO_URI || 'mongodb://127.0.0.1:27017/magnifica_db',

	'name': 'Magnifica',
	'brand': 'Magnifica',
	
	'static': 'client/public',
	'favicon': '/client/public/img/elem/favicon.png',
	'compress': true,
	'logger': '[:date[clf]] :method :url :status :response-time ms | :remote-addr :remote-user',
	
	'session': true,
	'session store': 'mongo',
	'cookie secret': process.env.COOKIE_SECRET || '',
	
	'auto update': true,
	'auth': true,
	'user model': 'Admin'

});

keystone.import('models');

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils
});

keystone.set('routes', require('./routes'));

keystone.set('nav', {
	'Промо': 'Promo',
	'Услуги': 'Services',
	'Прайсы': ['PricesLady', 'PricesBarbershop', 'PricesBrowBar', 'PricesPodology'],
	'Отзывы': 'Feedbacks',
	'Контакты': 'Contacts',
	'Пользователи': 'Admin'
});

keystone.start();
