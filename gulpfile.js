var gulp = require('gulp'),
	concat = require('gulp-concat'),
	sass = require('gulp-sass'),
	minifyCss = require('gulp-minify-css'),
	rename = require('gulp-rename'),
	autoprefixer = require('gulp-autoprefixer'),
	images = require('gulp-image-optimization'),
	jade = require('gulp-jade');


// Default task
gulp.task('default', ['minify', 'scripts', 'jade'], function() {});

// Watch
gulp.task('watch', ['default'], require('./tasks/watch'));

// -------------------------------------------------------

// SCSS compiling
gulp.task('sass', require('./tasks/sass'));

// Auto-prefixer
gulp.task('prefix', ['sass'], require('./tasks/prefix'));

// CSS Minifying
gulp.task('minify', ['prefix'], require('./tasks/minify'));

// Script concatenation
gulp.task('scripts', ['scripts--vendor', 'scripts--custom'], function() {});

gulp.task('scripts--vendor', require('./tasks/scripts--vendor'));
gulp.task('scripts--custom', require('./tasks/scripts--custom'));

// Jade to HTML
gulp.task('jade', require('./tasks/jade'));

// Images optimization
gulp.task('images', require('./tasks/images'));